package de.jensd.extractordemo;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Jens Deters
 */
public class YetAnotherDateUtil {

    public static long getDaysToNextBirthday(LocalDate dateOfBirth) {
        LocalDate nextBirthday = getNextBirthday(dateOfBirth);
        return ChronoUnit.DAYS.between(LocalDate.now(), nextBirthday);
    }

    public static LocalDate getNextBirthday(LocalDate dateOfBirth) {
        MonthDay birthdayOfMonth = MonthDay.from(dateOfBirth);
        LocalDate today = LocalDate.now();
        LocalDate birthdayThisYear = birthdayOfMonth.atYear(today.getYear());
        if (birthdayThisYear.isAfter(today) || birthdayThisYear.equals(today)) {
            return birthdayThisYear;
        }
        return birthdayThisYear.plusYears(1);
    }
}
