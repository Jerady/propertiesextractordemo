package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class LastNameComparator implements Comparator<PersonBean> {

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (o1 != null && o2 != null) {
            if (o1.getLastName() != null && o2.getLastName() != null) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Last Name";
    }

}
