package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class AgeComparator implements Comparator<PersonBean>{

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (o1 != null && o2 != null) {
            if (o1.getDateOfBirth().ageBinding().get()!= null && o2.getDateOfBirth().ageBinding().get()!= null) {
                return o1.getDateOfBirth().ageBinding().get().compareTo(o2.getDateOfBirth().ageBinding().get());
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Age";
    }    
}
