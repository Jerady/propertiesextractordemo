package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class DateOfBirthComparator implements Comparator<PersonBean> {

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (null != o1 && null != o2) {
            if (null != o1.getDateOfBirth() && null != o2.getDateOfBirth()) {
                if (null != o1.getDateOfBirth().getValue() && null != o2.getDateOfBirth().getValue()) {
                    return o1.getDateOfBirth().getValue().compareTo(o2.getDateOfBirth().getValue());
                }
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Date of Birth";
    }

}
