package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class FirstNameComparator implements Comparator<PersonBean> {

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (o1 != null && o2 != null) {
            if (null != o1.getFirstName() && null != o2.getFirstName()) {
                return o1.getFirstName().compareTo(o2.getFirstName());
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "First Name";
    }

}
