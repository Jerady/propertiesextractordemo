package de.jensd.extractordemo.data;

import java.time.format.DateTimeFormatter;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Callback;

/**
 *
 * @author Jens Deters
 */
public class PersonBean {

    private StringProperty firstName;
    private StringProperty lastName;
    private ObjectProperty<DateOfBirth> dateOfBirth;

    public PersonBean() {
    }

    public PersonBean(String firstName, String lastName, DateOfBirth dateOfBirth) {
        setFirstName(firstName);
        setLastName(lastName);
        setDateOfBirth(dateOfBirth);
    }

    public final StringProperty firstNameProperty() {
        if (firstName == null) {
            firstName = new SimpleStringProperty();
        }
        return firstName;
    }

    public final String getFirstName() {
        return firstNameProperty().get();
    }

    public final void setFirstName(final java.lang.String firstName) {
        firstNameProperty().set(firstName);
    }

    public final StringProperty lastNameProperty() {
        if (lastName == null) {
            lastName = new SimpleStringProperty();
        }
        return lastName;
    }

    public final java.lang.String getLastName() {
        return lastNameProperty().get();
    }

    public final void setLastName(final java.lang.String lastName) {
        lastNameProperty().set(lastName);
    }

    public final ObjectProperty<DateOfBirth> dateOfBirthProperty() {
        if (dateOfBirth == null) {
            dateOfBirth = new SimpleObjectProperty<>();
        }
        return dateOfBirth;
    }

    public final DateOfBirth getDateOfBirth() {
        return dateOfBirthProperty().get();
    }

    public final void setDateOfBirth(final DateOfBirth dateOfBirth) {
        dateOfBirthProperty().set(dateOfBirth);

    }

    public String stringValue() {
        return String.format("%s %s %s", getFirstName(), getLastName(), getDateOfBirth().getValue().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    public static Callback<PersonBean, Observable[]> extractor() {
        return (PersonBean p) -> new Observable[]{
            p.lastNameProperty(),
            p.firstNameProperty(),
            p.dateOfBirthProperty(),
            p.getDateOfBirth().ageBinding(),
            p.getDateOfBirth().daysToNextBirthdayBinding(),
            p.getDateOfBirth().nextBirthdayBinding()};
    }
}
