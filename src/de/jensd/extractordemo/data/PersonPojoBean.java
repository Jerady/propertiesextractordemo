package de.jensd.extractordemo.data;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.beans.Observable;
import javafx.util.Callback;

/**
 *
 * @author Jens Deters
 */
public class PersonPojoBean {

    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public PersonPojoBean() {
    }

    public PersonPojoBean(String firstName, String lastName, LocalDate birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        changes.firePropertyChange("firstName", this.firstName, this.firstName = firstName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        changes.firePropertyChange("lastName", this.lastName, this.lastName = lastName);
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        changes.firePropertyChange("birthday", this.birthday, this.birthday = birthday);
    }

    public String stringValue() {
        return String.format("%s %s %s", getFirstName(), getLastName(), getBirthday().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    public static Callback<PersonPojoBean, Observable[]> extractor() {
        return (PersonPojoBean arg0) -> new Observable[]{new PojoAdapter<>(arg0)};
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }

}
