package de.jensd.extractordemo.data;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Jens Deters
 */
public class DataSource {

    private final static String[] FIRST_NAMES = {"Alfons", "Petra", "Lia", "Mavie", "Neela", "Wieky", "Jens", "Peter", "Susi", "Siggi", "Heike", "Klaus", "Kiki", "Heinrich", "Thomas", "Ziggi"};
    private final static String[] LAST_NAMES = {"Stuckenbrock", "Müller-Lüdenscheit", "Klöbner", "Loose", "Maier", "Meier", "Mayer", "Brenigan", "Bramsmann", "Schepers", "Zameitat", "Powers", "Baumann", "Heiermann", "Kühne", "Dittmann"};
    private final static int[] YEARS = {1948, 1958, 1961, 1968, 1972, 1973, 1975, 1976, 1977, 1982, 1985, 1987, 1989, 1990};

    private static final Random RAND = new Random();

    public static String getRandomName() {
        return FIRST_NAMES[RAND.nextInt(FIRST_NAMES.length)];
    }

    public static String getRandomLastname() {
        return LAST_NAMES[RAND.nextInt(LAST_NAMES.length)];
    }

    public static LocalDate getRandomLocalDate() {
        return LocalDate.of(YEARS[RAND.nextInt(YEARS.length)], RAND.nextInt(12) + 1, RAND.nextInt(28) + 1);
    }

    public static ObservableList<PersonBean> getRandomPersonBeansList(int length) {
        ObservableList<PersonBean> persons = FXCollections.observableArrayList(PersonBean.extractor());

//        ObservableList<PersonBean> persons = FXCollections.observableArrayList(new Callback<PersonBean, Observable[]>() {
//            @Override
//            public Observable[] call(PersonBean p) {
//                return new Observable[]{
//                    p.lastNameProperty(),
//                    p.firstNameProperty(),
//                    p.birthdayProperty(),
//                    p.ageBinding()
//                };
//            }
//        });
//        ObservableList<PersonBean> persons = FXCollections.observableArrayList(
//                (PersonBean p) -> new Observable[]{
//                    p.lastNameProperty(), 
//                    p.firstNameProperty(), 
//                    p.birthdayProperty(), 
//                    p.ageBinding()}
//        );
        for (int i = 0;
                i < length;
                i++) {
            persons.add(new PersonBean(getRandomName(), getRandomLastname(), new DateOfBirth(getRandomLocalDate())));
        }
        return persons;
    }

    public static ObservableList<PersonPojoBean> getRandomPersonPojoBeansList(int length) {
        ObservableList<PersonPojoBean> persons = FXCollections.observableArrayList(PersonPojoBean.extractor());
        for (int i = 0; i < length; i++) {
            persons.add(new PersonPojoBean(getRandomName(), getRandomLastname(), getRandomLocalDate()));
        }
        return persons;
    }

    public static void main(String... args) {
        ObservableList<PersonBean> personsFX = getRandomPersonBeansList(200);
        personsFX.forEach(p -> {
            System.out.println(p.stringValue() + " next Birthday in " + getDaysToNextBirthday(p.getDateOfBirth().getValue()) + " days.");
        });

//        System.out.println("-------------------------------");
//
//        ObservableList<PersonPojoBean> persons = getRandomPersonPojoBeansList(200);
//        persons.forEach(p -> {
//            System.out.println(p.stringValue());
//        });
    }

    private static long getDaysToNextBirthday(LocalDate dateOfBirth) {
        LocalDate nextBirthday = getNextBirthday(dateOfBirth);
        return ChronoUnit.DAYS.between(LocalDate.now(), nextBirthday);

    }

    private static LocalDate getNextBirthday(LocalDate dateOfBirth) {
        MonthDay birthday = MonthDay.from(dateOfBirth);
        LocalDate today = LocalDate.now();
        LocalDate birthdayThisYear = birthday.atYear(today.getYear());
        if (birthdayThisYear.isAfter(today) || birthdayThisYear.equals(today)) {
            return birthdayThisYear;
        }
        return birthdayThisYear.plusYears(1);
    }
}
