package de.jensd.extractordemo.data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javafx.beans.value.ObservableValueBase;

/**
 * Adapt a Pojo to an Observable.
 * Note: extending ObservableValue is too much, but there is no ObservableBase ...
 *
 * @author Jeanette Winzenburg, Berlin
 * @param <T>
 */
public class PojoAdapter<T> extends ObservableValueBase<T>
{

   private final T bean;
   private PropertyChangeListener pojoListener;

   public PojoAdapter(T pojo)
   {
      this.bean = pojo;
      installPojoListener(pojo);
   }

   /**
    * Reflectively install a propertyChangeListener for the pojo, if available.
    * Silently does nothing if it cant.
    *
    * @param item
    */
   private void installPojoListener(T item)
   {
      try
      {
         Method method = item.getClass().getMethod("addPropertyChangeListener",
                                                   PropertyChangeListener.class);
         method.invoke(item, getPojoListener());
      }
      catch (NoSuchMethodException | SecurityException | IllegalAccessException |
            IllegalArgumentException | InvocationTargetException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * Returns the propertyChangeListener to install on each item.
    * Implemented to call notifyList.
    *
    * @return
    */
   private PropertyChangeListener getPojoListener()
   {
      if (pojoListener == null)
      {
         pojoListener = (PropertyChangeEvent evt) -> {
             fireValueChangedEvent();
         };
      }
      return pojoListener;
   }

   @Override
   public T getValue()
   {
      return bean;
   }

}