package de.jensd.extractordemo.cell;

import de.jensd.extractordemo.DateFormatter;
import de.jensd.extractordemo.data.DateOfBirth;
import java.time.LocalDate;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.util.StringConverter;

/**
 *
 * @author Jens Deters
 */
public class DatePickerTableCell<S, T> extends TableCell<S, DateOfBirth> {

    private DatePicker datePicker;

    public DatePickerTableCell() {
        setStyle("-fx-alignment: CENTER-RIGHT;");

        StringConverter converter = new StringConverter<LocalDate>() {

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return DateFormatter.DEFAULT_DATE.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, DateFormatter.DEFAULT_DATE);
                } else {
                    return null;
                }
            }
        };

        datePicker = new DatePicker();
        datePicker.setConverter(converter);

        datePicker.setOnAction(e -> {
            getItem().setValue(datePicker.getValue());
            commitEdit(getItem());
        });
        setGraphic(datePicker);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    }

    @Override
    protected void updateItem(DateOfBirth dateOfBirth, boolean empty) {
        super.updateItem(dateOfBirth, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            datePicker.setValue(dateOfBirth.getValue());
            setText(dateOfBirth.getValue().format(DateFormatter.DEFAULT_DATE));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

}
