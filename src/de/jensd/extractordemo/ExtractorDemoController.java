package de.jensd.extractordemo;

import de.jensd.extractordemo.cell.DatePickerTableCell;
import de.jensd.extractordemo.cell.DateTableCell;
import de.jensd.extractordemo.cell.PersonListCell;
import de.jensd.extractordemo.comparator.AgeComparator;
import de.jensd.extractordemo.comparator.DateOfBirthComparator;
import de.jensd.extractordemo.comparator.DaysToNextBirthdayComparator;
import de.jensd.extractordemo.comparator.FirstNameComparator;
import de.jensd.extractordemo.comparator.LastNameComparator;
import de.jensd.extractordemo.comparator.NextBirthdayComparator;
import de.jensd.extractordemo.data.DateOfBirth;
import de.jensd.extractordemo.data.PersonBean;
import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.control.GlyphCheckBox;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.time.LocalDate;
import java.util.Comparator;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;

public class ExtractorDemoController {

    @FXML
    private TableView<PersonBean> readOnlyTableView;
    @FXML
    private TableView<PersonBean> editableTableView;
    @FXML
    private ListView<PersonBean> readOnlyListView;
    @FXML
    private TableColumn<PersonBean, String> readOnlyFirstNameColumn;
    @FXML
    private TableColumn<PersonBean, String> readOnlyLastNameColumn;
    @FXML
    private TableColumn<PersonBean, Long> readOnlyAgeColumn;
    @FXML
    private TableColumn<PersonBean, LocalDate> readOnlyNextBirthdayColumn;
    @FXML
    private TableColumn<PersonBean, Long> readOnlyDaysToNextBirthdayColumn;
    @FXML
    private TableColumn<PersonBean, String> editableFirstNameColumn;
    @FXML
    private TableColumn<PersonBean, String> editableLastNameColumn;
    @FXML
    private TableColumn<PersonBean, DateOfBirth> editableBirthdayColumn;
    @FXML
    private TableColumn<PersonBean, Long> ageColumn;
    @FXML
    private ComboBox<Comparator<PersonBean>> personOrderByComboBox;
    @FXML
    private HBox orderBox;

    private DataModel model;

    private SortedList personSortedList;
    private ObservableList<Comparator<PersonBean>> personComparators;
    private GlyphCheckBox sortDirectionCheckBox;

    public void initialize() {
        model = new DataModel();
        GlyphIcon selectedIcon = new FontAwesomeIconView(FontAwesomeIcon.SORT_ALPHA_DESC);
        selectedIcon.setSize("2em");
        GlyphIcon notSelectedIcon = new FontAwesomeIconView(FontAwesomeIcon.SORT_ALPHA_ASC);
        notSelectedIcon.setSize("2em");
        sortDirectionCheckBox = new GlyphCheckBox(notSelectedIcon, selectedIcon, "");
        sortDirectionCheckBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            onChangeSortDirection();
        });

        orderBox.getChildren().add(sortDirectionCheckBox);
        personComparators = FXCollections.observableArrayList(new FirstNameComparator(),
                new LastNameComparator(),
                new DateOfBirthComparator(),
                new AgeComparator(),
                new DaysToNextBirthdayComparator(),
                new NextBirthdayComparator()
        );
        personOrderByComboBox.setItems(personComparators);
        personOrderByComboBox.disableProperty().bind(readOnlyListView.itemsProperty().isNull());
        readOnlyListView.disableProperty().bind(readOnlyListView.itemsProperty().isNull());
        readOnlyTableView.disableProperty().bind(readOnlyTableView.itemsProperty().isNull());
        editableTableView.disableProperty().bind(editableTableView.itemsProperty().isNull());
        sortDirectionCheckBox.disableProperty().bind(readOnlyListView.itemsProperty().isNull()
                .or(personOrderByComboBox.getSelectionModel().selectedItemProperty().isNull()));

        initListViews();
        initTableViews();
        onClear();
    }

    private void initListViews() {
        readOnlyListView.setCellFactory((ListView<PersonBean> param) -> {
            return new PersonListCell();
        });
    }

    private void initTableViews() {
        readOnlyFirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        readOnlyLastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        readOnlyAgeColumn.setCellValueFactory(i -> i.getValue().getDateOfBirth().ageBinding());
        readOnlyDaysToNextBirthdayColumn.setCellValueFactory(i -> i.getValue().getDateOfBirth().daysToNextBirthdayBinding());
        readOnlyNextBirthdayColumn.setCellValueFactory(i -> i.getValue().getDateOfBirth().nextBirthdayBinding());
        readOnlyNextBirthdayColumn.setCellFactory(c -> new DateTableCell());

        editableFirstNameColumn.setCellValueFactory(i -> i.getValue().firstNameProperty());
        editableLastNameColumn.setCellValueFactory(i -> i.getValue().lastNameProperty());
        editableBirthdayColumn.setCellValueFactory(i -> i.getValue().dateOfBirthProperty());
        ageColumn.setCellValueFactory(i -> i.getValue().getDateOfBirth().ageBinding());

        editableFirstNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        editableLastNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        editableBirthdayColumn.setCellFactory(c -> new DatePickerTableCell<>());
        editableBirthdayColumn.setOnEditCommit((TableColumn.CellEditEvent<PersonBean, DateOfBirth> t) -> {
            t.getRowValue().setDateOfBirth(t.getNewValue());
        });

        //editableTableView.setEditable(false);
        editableFirstNameColumn.setEditable(true);
        editableLastNameColumn.setEditable(true);
        editableBirthdayColumn.setEditable(true);

//
//        editableFirstNameColumn.setCellFactory(c -> {
//            TableCell<PersonBean, String> cell = new TableCell<PersonBean, String>() {
//                @Override
//                protected void updateItem(String item, boolean empty) {
//                    super.updateItem(item, empty);
//                    setText(item);
//                    if (item == null || empty) {
//                        setText(null);
//                        setGraphic(null);
//                    }
//                }
//            };
//            cell.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
//                if (event.getClickCount() > 1) {
//                    PersonBean myPerson = (PersonBean) cell.getTableView().getItems().get(cell.getIndex());
//                    myPerson.setFirstName("Got dblclick");
//                }
//            });
//            return cell;
//        });
    }

    @FXML

    public void onFillWithDemoDataFXBeans() {
        personSortedList = model.getPersonFXBeans().sorted();
        readOnlyListView.setItems(personSortedList);
        readOnlyTableView.setItems(personSortedList);
        editableTableView.setItems(model.getPersonFXBeans());
    }

    @FXML
    public void onFillWithDemoDataPojoBeans() {

    }

    @FXML
    public void onClear() {
        readOnlyListView.setItems(null);
        readOnlyTableView.setItems(null);
        editableTableView.setItems(null);
    }

    @FXML
    public void onSortByComparator() {
        personSortedList.setComparator(personOrderByComboBox.getSelectionModel().getSelectedItem());
    }

    @FXML
    public void onChangeSortDirection() {
        personSortedList.setComparator(personSortedList.getComparator().reversed());
    }

}
